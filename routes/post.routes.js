module.exports = app => {
  // Requires
    const posts = require("../controllers/posts.controllers.js");
    const router = require("express").Router();
  
    // Get all Posts
    router.get("/", posts.findAll);
    
    // Get a single Post with id
    router.get("/:id", posts.findOne);
    
    // Create a new Post
    router.post("/", posts.create);
  
    // Update a Post with id
    router.put("/:id", posts.update);
  
    // Delete a Post with id
    router.delete("/:id", posts.delete);
  
    app.use('/posts', router);
    
  };