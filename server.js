// Requires
const express = require('express');
const bodyParser = require('body-parser');
const cors = require("cors");
// Sequialixe requires
const db = require("./models");
db.sequelize.sync();

// Var
const app = express();

// CORS
var corsOptions = {
  origin: "http://localhost:8081"
};
app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// Simple Route
app.get('/', (req, res) => {
  res.status(200).json({
    ok: true,
    message: 'Bienvenido a /'
  })
});

require("./routes/post.routes")(app);

// Petitions
// app.listen(process.env.PORT, process.env.YOUR_HOST, () => {
    app.listen(3000, ()=>{
        console.log('Server ON en puerto 3000');
      });
    