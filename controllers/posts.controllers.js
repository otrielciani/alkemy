// Requires
const db = require("../models");
const path = require('path');

const Post = db.posts;
const Op = db.Sequelize.Op;


// Create and Save a new Post
exports.create = (req, res) => {
  // Validate request
  if (!req.body.title) {
    res.status(400).send({
      message: "Title can not be empty!"
    });
    return;
  }
  if (!req.body.content) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }
  if (!req.body.class) {
    res.status(400).send({
      message: "Class can not be empty!"
    });
    return;
  }

  // Create a Post

  const post = {
    title: req.body.title,
    content: req.body.content,
    images: req.body.images ? req.body.images : 'https://www.wallchase.com.ar/assets/img/no-pic.jpg',
    class: req.body.class
  };
  
  // Extensions URL
  const extensionsAvailable = ['png', 'jpg', 'gif', 'jpeg' ]
  const ext = path.extname(post.images||'').split('.');
  if ( extensionsAvailable.indexOf( ext[ext.length - 1] ) < 0 ) {
    return res.status(400).json({
      ok: false,
      message: 'Invalid url',
      errors: { message: 'You must select a url with extension: ' + extensionsAvailable.join(', ' ) }
    })
  }
  
  // Save Post in the database
  Post.create(post)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Post."
      });
    });
};

// Retrieve all Posts from the database.
exports.findAll = (req, res) => {

  Post.findAll({attributes: ["title", "class", "images", "createdAt", "id"], order: [["createdAt", "DESC"]]})
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving posts."
      });
    });
};

// Find a single Post with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Post.findByPk(id, {attributes: ["title", "content", "class" , "images", "createdAt", "id"]})
    .then(data => {
      if ( data === null ) {
        res.status(404).send({
          message: `Cannot find Post with id = ${id}`
        })
        return;
      }
      res.send(data);
    })
    .catch(err => {
      console.log(err);
      res.status(500).send({
        message: "Error retrieving Post with id = " + id
      });
    });
};

// Update a Post by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  Post.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Post was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update Post with id = ${id}. Post was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Post with id = " + id
      });
    });
};

// Delete a Post with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Post.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Post was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Post with id = ${id}. Post was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Post with id = " + id
      });
    });
};
